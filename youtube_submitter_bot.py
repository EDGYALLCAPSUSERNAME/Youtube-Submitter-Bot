import time
import praw
import OAuth2Util
from datetime import datetime
from apiclient.discovery import build

# User config
# --------------------------------------------------------------------
DEVELOPER_KEY = ''
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'

SUBREDDIT_NAME = ''

CHANNEL_IDS = []

# How long you want it to wait before searching for
# videos again in seconds (default is an hour)
# set this to False if you don't want it to wait
# and exit after posting videos (so you can run on your own schedule)
WAIT_TIME = 3600

# Set how many results you want it to return
# reccommended is 1, set higher if your channels
# produce a lot of videos or won't run this as frequently
MAX_RESULTS = 1

# Set true if you want the channel name in the title
ADD_CHANNEL_NAME_TO_TITLE = False

# Set true if you want link flairs added to post
ADD_LINK_FLAIR_TO_POSTS = False

# Set true if you want channel added to css class
# E.G. FlairCSSClass-ChannelName
APPEND_CHANNEL_TO_CSS_CLASS = False
LINK_FLAIR_CSS_CLASS = ''

# Set true if you want channel name as flair text
CHANNEL_NAME_AS_FLAIR_TEXT = False

# Else set static flair text
LINK_FLAIR_TEXT = ''

# --------------------------------------------------------------------


def add_flair_to_post(subm, channel_name):
    if not LINK_FLAIR_TEXT and not CHANNEL_NAME_AS_FLAIR_TEXT:
        print('ERROR: You have\'t set your LINK_FLAIR_TEXT')
        print('Please set LINK_TEXT or ' +
              'set CHANNEL_NAME_AS_FLAIR_TEXT to True' +
              ' and try running this again')
        print('Deleting post and exiting...')
        subm.delete()
        exit(1)
    if not LINK_FLAIR_CSS_CLASS:
        print('ERROR: You have\'t set your LINK_FLAIR_CSS_CLASS')
        print('Please set it and try running this again')
        print('Deleting post and exiting...')
        subm.delete()
        exit(1)

    if APPEND_CHANNEL_TO_CSS_CLASS:
        flair_class = LINK_FLAIR_CSS_CLASS + '-' + channel_name
    else:
        flair_class = LINK_FLAIR_CSS_CLASS

    if CHANNEL_NAME_AS_FLAIR_TEXT:
        flair_text = channel_name
    else:
        flair_text = LINK_FLAIR_TEXT

    print('Adding flair...')
    subm.set_flair(flair_css_class=flair_class,
                   flair_text=flair_text)


def post_to_reddit(r, video):
    if ADD_CHANNEL_NAME_TO_TITLE:
        title = '[{}] {}'.format(video[2], video[0])
    else:
        title = video[0]

    link = 'https://www.youtube.com/watch?v={}'.format(video[1])

    try:
        print('Submitting {}...'.format(title))
        subm = r.submit(SUBREDDIT_NAME, title, url=link, captcha=None)
    except praw.errors.AlreadySubmitted:
        print('Already submitted video, continuing...')

    if ADD_LINK_FLAIR_TO_POSTS:
        add_flair_to_post(subm, video[2])


def youtube_search(channel_ID):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey=DEVELOPER_KEY)

    search_response = youtube.search().list(
                        channelId=channel_ID,
                        order='date',
                        part='id, snippet',
                        maxResults=MAX_RESULTS,
                        type='video').execute()

    videos = []
    for search_result in search_response.get('items', []):
        videos.append([search_result['snippet']['title'],
                       search_result['id']['videoId'],
                       search_result['snippet']['channelTitle'],
                       search_result['snippet']['publishedAt']])

    return videos


def date_is_newer(d1, d2):
    d1 = datetime(int(d1[:4]), int(d1[5:7]), int(d1[8:10]),
                  int(d1[11:13]), int(d1[14:16]))
    d2 = datetime(int(d2[:4]), int(d2[5:7]), int(d2[8:10]),
                  int(d2[11:13]), int(d2[14:16]))

    if d1 > d2:
        return True
    else:
        return False


def sort_by_published(videos):
    while True:
        did_swap = False
        for i in range(1, len(videos)):
            if date_is_newer(videos[i - 1][3], videos[i][3]):
                videos[i - 1], videos[i] = videos[i], videos[i - 1]
                did_swap = True

        if not did_swap:
            break

    return videos


def main():
    r = praw.Reddit(user_agent='Youtube_Poster v2.0 /u/EDGYALLCAPSUSERNAME')
    o = OAuth2Util.OAuth2Util(r, print_log=True)

    while True:
        o.refresh()

        try:
            print('Getting video(s)...')
            videos = []
            for channel in CHANNEL_IDS:
                c_videos = youtube_search(channel)
                [videos.append(v) for v in c_videos]

            if len(videos) > 1:
                videos = sort_by_published(videos)

            for video in videos:
                post_to_reddit(r, video)

            if WAIT_TIME:
                print('Sleeping...')
                time.sleep(WAIT_TIME)
            else:
                print('Exiting...')
                exit(0)
        except Exception as e:
            print('ERROR: ' + str(e))
            time.sleep(500)


if __name__ == '__main__':
    # try to import my testing config
    try:
        import bot
        SUBREDDIT_NAME = bot.SUBREDDIT_NAME
        DEVELOPER_KEY = bot.DEVELOPER_KEY
        CHANNEL_IDS = bot.CHANNEL_IDS
    except ImportError:
        pass

    if not DEVELOPER_KEY:
        print('ERROR: You need to add your google developer key before ' +
              'this can run')
        print('Exiting...')
        exit(1)
    if not SUBREDDIT_NAME:
        print('ERROR: You need to add a subreddit name before this can run')
        print('Exiting...')
        exit(1)
    if not CHANNEL_IDS:
        print('ERROR: You need to add channel ids before this can run')
        print('Exiting...')
        exit(1)

    main()
